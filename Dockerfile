FROM node:18-alpine

WORKDIR /app

RUN apk update && apk upgrade
RUN apk add git

COPY ./package*.json /app/

RUN npm install --legacy-peer-deps && npm cache clean --force

COPY . .

ENV PATH ./node_modules/.bin/:$PATH

RUN npm run build

EXPOSE 3000

ENV NUXT_HOST=0.0.0.0

ENV NUXT_PORT=3000

CMD [ "npm", "start" ]

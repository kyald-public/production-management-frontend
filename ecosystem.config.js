module.exports = {
  apps: [
    {
      name: 'ProductionMGMT',
      port: '3222',
      exec_mode: 'cluster',
      instances: 'max',
      script: './.output/server/index.mjs',
    },
  ],
}

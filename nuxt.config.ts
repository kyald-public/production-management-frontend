import { createResolver } from '@nuxt/kit'
import presetIcons from '@unocss/preset-icons'

const { resolve } = createResolver(import.meta.url)

export default defineNuxtConfig({

  ssr: true,

  experimental: {
    renderJsonPayloads: false,
  },

  srcDir: 'src',
  devtools: true,
  runtimeConfig: {
    public: {
      BASE_API_URL: process.env.BASE_API_URL,
      X_API_KEY: process.env.X_API_KEY,
      GOOGLE_APPLICATION_CREDENTIALS: process.env.GOOGLE_APPLICATION_CREDENTIALS,
    },
  },

  plugins: [
    // { src: 'bootstrap/dist/js/bootstrap.min.js',  mode: 'client' }
    // { src: '~/plugins/vue-chart.js', mode: 'client' },
  ],

  modules: [
    '@vueuse/nuxt',
    '@nuxtjs/tailwindcss',
    // pinia plugin - https://pinia.esm.dev
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt',
    // unocss plugin - https://github.com/unocss/unocss
    '@unocss/nuxt',
    '@nuxtjs/i18n',
    '@nuxtjs/color-mode',
    // https://github.com/huntersofbook/huntersofbook/tree/main/packages/naive-ui-nuxt
    '@huntersofbook/naive-ui-nuxt',
    'nuxt-vue3-google-signin',

  ],
  css: [
    // resolve('./assets/scss/_variables.scss'),
    resolve('./src/assets/scss/app.scss'),
  ],

  build: {
    transpile: ['@headlessui/vue', '@meforma/vue-toaster', 'vue-chartjs', 'chart.js'],
  },
  unocss: {
    uno: false,
    preflight: false,
    icons: true,
    presets: [
      presetIcons({
        scale: 1.2,
        extraProperties: {
          display: 'inline-block',
        },
      }),
    ],
    safelist: ['i-twemoji-flag-us-outlying-islands', 'i-twemoji-flag-turkey'],
  },

  // localization - i18n config
  i18n: {
    locales: [
      {
        code: 'en',
        file: 'en-US.json',
      },
      { code: 'tr', file: 'tr-TR.json' },
    ],
    defaultLocale: 'tr',
    lazy: true,
    langDir: 'locales/',
    strategy: 'prefix_except_default',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected',
      redirectOn: 'root', // recommended
    },
    // vueI18n: {
    //   legacy: false,
    //   locale: 'tr',
    //   fallbackLocale: 'tr',
    //   availableLocales: ['en', 'tr'],
    // },
  },

  imports: {
    dirs: [resolve('./src/stores'), '~/stores', resolve('./src/types'), '~/types'],
  },

  // module::pinia
  pinia: {
    autoImports: [['defineStore', 'definePiniaStore']],
  },

  googleSignIn: {
    clientId: '936456543051-rr4u6pru683e8gaeen2iio64cbdrjisc.apps.googleusercontent.com',
  },

  typescript: {
    tsConfig: {
      compilerOptions: {
        strict: true,
        types: ['@pinia/nuxt', './type.d.ts'],
      },
    },
  },
  colorMode: {
    classSuffix: '',
    fallback: 'light',
    storageKey: 'color-mode',
  },

  tailwindcss: {
    configPath: './tailwind.config.js',
  },

  vite: {
    logLevel: 'info',
  },

  postcss: {
    plugins: {
      'postcss-nested': {},
      'postcss-import': {},
      'tailwindcss/nesting': {},
      'tailwindcss': {},
    },
  },
})

import VueFeather from 'vue-feather'
import Multiselect from 'vue-multiselect'
import 'vue-multiselect/dist/vue-multiselect.css'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css'
import PerfectScrollbar from 'vue3-perfect-scrollbar'
import 'vue3-perfect-scrollbar/dist/vue3-perfect-scrollbar.css'
import VueApexCharts from 'vue3-apexcharts'
import MoneyFormat from 'vue-money-format'

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component('vue-feather', VueFeather)
  nuxtApp.vueApp.component('multiselect', Multiselect)
  nuxtApp.vueApp.component('money-format', MoneyFormat)
  nuxtApp.vueApp.use(PerfectScrollbar)
  nuxtApp.vueApp.use(VueSweetalert2)
  // nuxtApp.vueApp.use(VueApexCharts);
})

/* eslint-disable eqeqeq */
import { storeToRefs } from 'pinia' // import storeToRefs helper hook from pinia
import AuthModule from '@/repository/modules/auth'
import ProfileModule from '@/repository/modules/profile'
import Helpers from '@/utils/helpers'
import { IAuthEmailRequest, IAuthSocialRequest } from '~/types/request/authRequest'
import { IBase, IBaseError } from '~/types/response/base'
import { IUserData } from '~/types/response/auth'

export default defineNuxtPlugin((nuxtApp) => {
  // Import Modules
  const auth = useAuthStore()
  const prefs = usePrefStore()
  const router = useRouter()
  const helpers = new Helpers()
  const authModule = new AuthModule()
  const profileModule = new ProfileModule()
  const { loading, hasError, errorData } = storeToRefs(auth) // make authenticated state reactive with storeToRefs

  async function authenticateUserEmail(authRequest: IAuthEmailRequest) {
    prefs.resetData()
    auth.resetData()

    loading.value = true

    const {
      data,
      pending,
      error,
    } = await authModule.loginWithEmail(authRequest)

    loading.value = pending.value

    hasError.value = false

    // if ((error.value as any) && (error.value as any).data.code == 401)
    //   prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      const userData = data.value.data as IUserData

      helpers.toast('success', data.value?.message)

      prefs.authenticated = true
      prefs.accessToken = userData.token as string

      await checkAccountStatus()
    }
  }

  async function authenticateUserSocial(authRequest: IAuthSocialRequest) {
    prefs.resetData()
    auth.resetData()

    const authModule = new AuthModule()

    loading.value = true

    const {
      data,
      pending,
      error,
    } = await authModule.signinSocialLogin(authRequest)

    loading.value = pending.value
    hasError.value = false

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      const userData = data.value.data as IUserData

      helpers.toast('success', data.value?.message)

      prefs.authenticated = true
      prefs.accessToken = userData.token as string

      await checkAccountStatus()
    }
  }

  async function signupUserSocial(authRequest: IAuthSocialRequest) {
    prefs.resetData()
    auth.resetData()

    const authModule = new AuthModule()

    loading.value = true

    const {
      data,
      pending,
      error,
    } = await authModule.signupSocialLogin(authRequest)

    loading.value = pending.value
    hasError.value = false

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      const userData = data.value.data as IUserData

      helpers.toast('success', data.value?.message)

      prefs.authenticated = true
      prefs.accessToken = userData.token as string

      await checkAccountStatus()
    }
  }

  async function signupUserEmail(authRequest: IAuthEmailRequest) {
    prefs.resetData()
    auth.resetData()

    loading.value = true

    const {
      data,
      pending,
      error,
    } = await authModule.signupWithEmail(authRequest)

    loading.value = pending.value

    hasError.value = false

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      const userData = data.value.data as IUserData

      helpers.toast('success', data.value?.message)

      prefs.authenticated = true
      prefs.accessToken = userData.token as string

      await checkAccountStatus()
    }
  }

  async function checkAccountStatus() {
    const {
      data,
      pending,
      error,
    } = await profileModule.getProfileData()

    loading.value = pending.value
    hasError.value = false

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
    }

    if (data.value?.code == 200) {
      const user = data.value?.data as IUserData

      prefs.userEmail = user.email as string
      prefs.userName = user.name as string
      // prefs.authenticated = true;
    }
  }

  function logUserOut() {
    prefs.resetData()
    router.push('/auth')
  }

  return {
    provide: {
      apiAuth: {
        authenticateUserEmail,
        authenticateUserSocial,
        signupUserSocial,
        signupUserEmail,
        logUserOut,
        checkAccountStatus,
      },
    },
  }
})

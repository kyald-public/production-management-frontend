/* eslint-disable eqeqeq */
import { storeToRefs } from 'pinia' // import storeToRefs helper hook from pinia
import AuthModule from '@/repository/modules/auth'
import InventoryModule from '~/repository/modules/misc'
import Helpers from '@/utils/helpers'
import { IAuthEmailRequest, IAuthSocialRequest } from '~/types/request/authRequest'
import { IBase, IBaseError } from '~/types/response/base'
import { IUserData } from '~/types/response/auth'
import { IProfileUpdateRequest } from '~/types/request/profileRequest'
import { IPaginationRequest } from '~/types/request/paginationRequest'
import { ICityData, IProvinceData, IRoleData } from '~/types/response/misc'
import { useMiscStore } from '~/stores/use-misc'
import { IMiscCityRequest } from '~/types/request/miscRequest'

export default defineNuxtPlugin((nuxtApp) => {
  // Import Modules
  const prefs = usePrefStore()
  const router = useRouter()
  const helpers = new Helpers()
  const miscModule = new InventoryModule()
  const { loading, hasError, errorData, roleListData, cityListData, provinceListData } = storeToRefs(useMiscStore()) // make authenticated state reactive with storeToRefs
  async function getRoleList(request: IPaginationRequest) {
    loading.value = true

    const {
      data,
      pending,
      error,
    } = await miscModule.getRoleList(request)

    loading.value = pending.value

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      hasError.value = false
      roleListData.value = (data.value.data as any) as [IRoleData]
    }
  }

  async function getCityList(request: IMiscCityRequest) {
    loading.value = true

    const {
      data,
      pending,
      error,
    } = await miscModule.getCityList(request)

    loading.value = pending.value

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      hasError.value = false
      cityListData.value = (data.value.data as any) as [ICityData]
    }
  }

  async function getProvinceList(request: IPaginationRequest) {
    loading.value = true

    const {
      data,
      pending,
      error,
    } = await miscModule.getProvinceList(request)

    loading.value = pending.value

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      hasError.value = false
      provinceListData.value = (data.value.data as any) as [IProvinceData]
    }
  }

  return {
    provide: {
      apiMisc: {
        getRoleList,
        getCityList,
        getProvinceList,
      },
    },
  }
})

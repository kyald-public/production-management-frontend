/* eslint-disable eqeqeq */
import { storeToRefs } from 'pinia' // import storeToRefs helper hook from pinia
import AuthModule from '@/repository/modules/auth'
import InventoryModule from '~/repository/modules/user'
import Helpers from '@/utils/helpers'
import { IAuthEmailRequest, IAuthSocialRequest } from '~/types/request/authRequest'
import { IBase, IBaseError } from '~/types/response/base'
import { IProfileUpdateRequest } from '~/types/request/profileRequest'
import { IPaginationRequest } from '~/types/request/paginationRequest'
import { IUserListData, IUserListDataPagination } from '~/types/response/user'
import { IUserAddRequest, IUserShowRequest, IUserUpdateRequest } from '~/types/request/userRequest'
import { useUserStore } from '~/stores/use-user'

export default defineNuxtPlugin((nuxtApp) => {
  // Import Modules
  const prefs = usePrefStore()
  const router = useRouter()
  const helpers = new Helpers()
  const inventoryModule = new InventoryModule()
  const { loading, hasError, errorData, userListData, userData, userList } = storeToRefs(useUserStore()) // make authenticated state reactive with storeToRefs

  async function addUser(request: IUserAddRequest) {
    loading.value = true

    const {
      data,
      pending,
      error,
    } = await inventoryModule.addUser(request)

    loading.value = pending.value

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 201) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 201) {
      hasError.value = false
      helpers.toast('success', data.value.message)
    }
  }

  async function updateUser(request: IUserUpdateRequest) {
    loading.value = true

    const {
      data,
      pending,
      error,
    } = await inventoryModule.updateUser(request)

    loading.value = pending.value

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 201) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 201) {
      hasError.value = false
      helpers.toast('success', data.value.message)
    }
  }

  async function getUser(request: IUserShowRequest) {
    loading.value = true

    const {
      data,
      pending,
      error,
    } = await inventoryModule.getUser(request)

    loading.value = pending.value

    if ((error.value as any) && (error.value as any).data.code == 401)
      prefs.logout()

    if (data.value?.code != 200) {
      const baseError = (error.value as any).data as IBaseError

      hasError.value = true
      helpers.toast('error', baseError.message)
      errorData.value = Object(baseError)
    }

    if (data.value?.code == 200) {
      hasError.value = false
      userData.value = (data.value.data as any) as IUserListData
    }
  }

  return {
    provide: {
      apiUser: {
        addUser,
        updateUser,
        getUser,
      },
    },
  }
})

import Helpers from '@/utils/helpers'
import { IUserListData, IUserListDataPagination } from '~/types/response/user'

const helpers = new Helpers()

export const useUserStore = definePiniaStore('user', {
  state: () => ({
    loading: false,
    hasError: false,
    userListData: {} as IUserListDataPagination,
    userData: {} as IUserListData,
    userList: [] as IUserListData[],
    errorData: Object,
  }),

  getters: {
    getUserList(): IUserListDataPagination {
      return this.userListData
    },
  },

  actions: {
    setUserListData(userData: IUserListDataPagination) {
      this.userListData = userData
    },
    handleError(field: string) {
      if (!this.hasError)
        return

      return helpers.handleErrorMessage(this.errorData, field)
    },
  },
})

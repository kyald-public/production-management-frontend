import { useStorage } from '@vueuse/core'
import Helpers from '@/utils/helpers'

const helpers = new Helpers()

export const usePrefStore = definePiniaStore('pref', {
  state: () => ({
    authenticated: useStorage('authenticated', false),
    accessToken: useStorage('token', ''),
    userName: useStorage('username', ''),
    userEmail: useStorage('useremail', ''),
    activeTab: useStorage('activetab', ''),
    activeRoute: useStorage('activeRoute', ''),
  }),
  persist: true,
  actions: {
    logout() {
      setTimeout(() => {
        const router = useRouter()
        this.resetData()
        router.push('/auth')
      }, 1000)
    },

    resetData() {
      this.authenticated = false
      this.accessToken = ''
      this.activeTab = ''
      this.activeRoute = ''
      this.userName = ''
      this.userEmail = ''
    },
  },
})

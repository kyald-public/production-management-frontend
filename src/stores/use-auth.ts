import { defineStore } from 'pinia'
import { $fetch, FetchOptions } from 'ofetch'

import { storeToRefs } from 'pinia' // import storeToRefs helper hook from pinia
import { useStorage } from '@vueuse/core'

import AuthModule from '@/repository/modules/auth'
import ProfileModule from '@/repository/modules/profile'

import Helpers from '@/utils/helpers'

const helpers = new Helpers()

export const useAuthStore = definePiniaStore('auth', {
  state: () => ({
    loading: false,
    hasError: false,
    errorData: Object,
  }),
  actions: {
    resetData() {
      this.loading = false
      this.hasError = false
      this.errorData = Object()
    },
    handleError(field: string) {
      if (!this.hasError)
        return

      return helpers.handleErrorMessage(this.errorData, field)
    },
  },
})

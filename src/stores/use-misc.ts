import Helpers from '@/utils/helpers'
import { ICityData, IProvinceData, IRoleData } from '~/types/response/misc'

const helpers = new Helpers()

export const useMiscStore = definePiniaStore('misc', {
  state: () => ({
    loading: false,
    hasError: false,
    roleListData: [] as object as [IRoleData],
    cityListData: [] as object as [ICityData],
    provinceListData: [] as object as [IProvinceData],
    errorData: Object,
  }),

  getters: {

  },

  actions: {
    handleError(field: string) {
      if (!this.hasError)
        return

      return helpers.handleErrorMessage(this.errorData, field)
    },
  },
})

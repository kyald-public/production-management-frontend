/* eslint-disable eqeqeq */
import { createToaster } from '@meforma/vue-toaster'

const toaster = createToaster({ /* options */ })

class Helpers {
  public toast(type: string, message: string) {
    toaster.show(message, {
      type,
      maxToasts: 3,
      queue: false,
    })
  }

  public handleErrorMessage(errorData: any, field: string) {
    if (errorData.data != undefined) {
      const error = errorData.data.filter((elem: any) => {
        return (elem.field == field)
      })
      if (error.length > 0 && error[0].field === field)
        return error[0].message
      else
        return ''
    }
  }

  public currencyFormatter(price: number) {
    const formatPrice = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
    })

    return formatPrice.format(price)
  }
}

export default Helpers

import { IPaginationResponse } from './pagination'

export interface IUserRoles {
  name: string
}

export interface IUserOutlets {
  name: string
  user_outlet_id: number
  uuid: string
  outlet_id: number
}
export interface IUserListData {
  id: number
  uuid: string
  name: string
  username: string
  phone: string
  email: string
  roles: IUserRoles[]
}

export interface IUserListDataPagination extends IPaginationResponse {
  data: IUserListData[]
}

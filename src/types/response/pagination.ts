export interface IPaginationResponse {
  current_page: number
  from: number
  last_page: number
  per_page: number
  to: number
  total: number
}

export const PaginationData = ref<IPaginationResponse>({
  current_page: 0,
  from: 0,
  last_page: 0,
  per_page: 0,
  to: 0,
  total: 0,
})

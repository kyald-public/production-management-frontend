export interface IRoleData {
  id: number
  name: string
  guard_name: string
}

export interface ICityData {
  city_id: number
  name: string
}

export interface IProvinceData {
  province_id: number
  name: string
}

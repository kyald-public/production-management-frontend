export interface IBase {
  code: number
  message: string
  timestamp: string
  data: object
}

export const Base = ref<IBase>({
  code: 0,
  message: '',
  timestamp: '',
  data: Object,
})

export interface IBaseError {
  code: number
  message: string
  timestamp: string
  data: [{
    field: string
    message: string
  }]
}

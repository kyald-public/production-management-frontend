import { ref } from 'vue'

export interface IUserData {
  id: number
  token: string
  uuid: string
  name: string
  phone: string
  email: string
  created_at: string
  updated_at: string
  verified_at: string
}

export const UserData = ref<IUserData>({
  id: 0,
  token: '',
  uuid: '',
  name: '',
  phone: '',
  email: '',
  created_at: '',
  updated_at: '',
  verified_at: '',

})

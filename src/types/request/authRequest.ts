export interface IAuthEmailRequest {
  username: string
  password: string
}

export interface IAuthSocialRequest {
  provider: string
  access_token: string
}

// Getter

export const authEmailRequest = ref<IAuthEmailRequest>({
  username: '',
  password: '',
})

export const authSocialRequest = ref<IAuthSocialRequest>({
  access_token: '',
  provider: '',
})

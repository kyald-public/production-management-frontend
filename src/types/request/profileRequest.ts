export interface IProfileUpdateRequest {
  name: string
  phone: string
}

// Getter

export const profileUpdateRequest = ref<IProfileUpdateRequest>({
  name: '',
  phone: '',
})

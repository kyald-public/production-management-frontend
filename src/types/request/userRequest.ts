import { IPaginationRequest } from './paginationRequest'

export interface IUserAddRequest {
  username: string
  name: string
  email: string
  phone: string
  password: string
  pin: string
  role: string
}

export interface IUserUpdateRequest {
  id: number
  username: string
  name: string
  email: string
  phone: string
  password: string
  pin: string
  role: string
}

export interface IUserShowRequest {
  by_user_id: number
}

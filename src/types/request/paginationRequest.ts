export interface IPaginationRequest {
  page: number
  paginate: boolean
}

// Getter
export const paginationRequest = ref<IPaginationRequest>({
  page: 1,
  paginate: true,
})

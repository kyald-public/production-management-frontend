import { IPaginationRequest } from './paginationRequest'

export interface IMiscCityRequest extends IPaginationRequest {
  province_id: number
}

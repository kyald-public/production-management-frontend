// 3rd's
import { FetchOptions } from 'ofetch'
import FetchFactory from '../factory'
import { AsyncDataOptions } from '#app'

// locals
import { IBase } from '~/types/response/base'
import { IPaginationRequest } from '~/types/request/paginationRequest'

interface IMisc {
}

class MiscModule extends FetchFactory<IBase> {
  private RESOURCE = '/private'

  /**
     * Return the miscs as array
     * @param asyncDataOptions options for `useAsyncData`
     * @returns
     */
  async getRoleList(request: IPaginationRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/miscs/list-role`,
                    request, // body
                    undefined, // params
                    fetchOptions,
        )
      },
    )
  }

  async getCityList(request: IPaginationRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/miscs/list-city`,
                    request, // body
                    undefined, // params
                    fetchOptions,
        )
      },
    )
  }

  async getProvinceList(request: IPaginationRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/miscs/list-province`,
                    request, // body
                    undefined, // params
                    fetchOptions,
        )
      },
    )
  }
}

export default MiscModule

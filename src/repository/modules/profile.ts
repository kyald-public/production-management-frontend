// 3rd's
import { FetchOptions } from 'ofetch'
import FetchFactory from '../factory'
import { AsyncDataOptions } from '#app'

// locals
import { IBase } from '~/types/response/base'
import { IProfileUpdateRequest } from '~/types/request/profileRequest'

interface IProfile {
}

class ProfileModule extends FetchFactory<IBase> {
  private RESOURCE = '/private/profiles'

  /**
     * Return the products as array
     * @param asyncDataOptions options for `useAsyncData`
     * @returns
     */
  async getProfileData() {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'GET',
                    `${this.RESOURCE}/show-profile`,
                    undefined, // body
                    fetchOptions,
        )
      },
    )
  }

  async updateProfileData(updateProfileData: IProfileUpdateRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/update-profile`,
                    updateProfileData, // body
                    fetchOptions,
        )
      },
    )
  }
}

export default ProfileModule

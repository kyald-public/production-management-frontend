// 3rd's
import { FetchOptions } from 'ofetch'
import FetchFactory from '../factory'
import { AsyncDataOptions } from '#app'

// locals
import { IBase } from '~/types/response/base'
import { IPaginationRequest } from '~/types/request/paginationRequest'
import { IUserAddRequest, IUserShowRequest, IUserUpdateRequest } from '~/types/request/userRequest'

interface IUser {
}

class UserModule extends FetchFactory<IBase> {
  private RESOURCE = '/private'

  /**
     * Return the products as array
     * @param asyncDataOptions options for `useAsyncData`
     * @returns
     */
  async getUserList(request: IPaginationRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/users/list-user`,
                    request, // body
                    undefined, // params
                    fetchOptions,
        )
      },
    )
  }

  async addUser(request: IUserAddRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/users/add-user`,
                    request, // body
                    fetchOptions,
        )
      },
    )
  }

  async getUser(request: IUserShowRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/users/show-user`,
                    request, // body
                    fetchOptions,
        )
      },
    )
  }

  async updateUser(request: IUserUpdateRequest) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Additional Headers
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/users/update-user`,
                    request, // body
                    fetchOptions,
        )
      },
    )
  }
}

export default UserModule

// 3rd's
import { FetchOptions } from 'ofetch'
import FetchFactory from '../factory'
import { AsyncDataOptions } from '#app'
import { IAuthEmailRequest, IAuthSocialRequest } from '~/types/request/authRequest'
import { IBase } from '~/types/response/base'

// locals

class AuthModule extends FetchFactory<IBase> {
  private RESOURCE = '/auth'

  /**
     * Return the products as array
     * @param asyncDataOptions options for `useAsyncData`
     * @returns
     */
  async loginWithEmail(
    authRequest: IAuthEmailRequest,
  ) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Custom Header
        }

        return this.call(
          'POST',
                    `${this.RESOURCE}/login`,
                    authRequest, // body
                    fetchOptions,
        )
      },
    )
  }

  async signinSocialLogin(
    authRequest: IAuthSocialRequest,
  ) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Custom Header
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/login/social-check`,
                    authRequest, // body
                    fetchOptions,
        )
      },
    )
  }

  async signupWithEmail(
    authRequest: IAuthEmailRequest,
  ) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Custom Header
        }

        return this.call(
          'POST',
                    `${this.RESOURCE}/register`,
                    authRequest, // body
                    fetchOptions,
        )
      },
    )
  }

  async signupSocialLogin(
    authRequest: IAuthSocialRequest,
  ) {
    return useAsyncData<IBase>(
      () => {
        const fetchOptions: FetchOptions<'json'> = {
          // Custom Header
        }
        return this.call(
          'POST',
                    `${this.RESOURCE}/register/social`,
                    authRequest, // body
                    fetchOptions,
        )
      },
    )
  }
}

export default AuthModule

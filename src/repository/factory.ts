// 3rd's
import { $Fetch, $fetch, FetchOptions } from 'ofetch'

/*
 The HttpFactory acts as a wrapper around an HTTP client.
 It encapsulates the functionality for making API requests asynchronously
 through the call function, utilizing the provided HTTP client.
*/
class FetchFactory<T> {
  private $_fetch: $Fetch
  private $_fetchFormData: $Fetch

  constructor() {
    const runtimeConfig = useRuntimeConfig()

    const fetchOptions: FetchOptions = {
      baseURL: runtimeConfig.public.BASE_API_URL,
      headers: {
        'Accept-Language': 'en-US',
        'Accept': 'application/json',
        'x-api-key': runtimeConfig.public.X_API_KEY,
        'Content-Type': 'application/json',
      },

    }
    this.$_fetch = $fetch.create(fetchOptions)

    const fetchOptionsFormData: FetchOptions = {
      baseURL: runtimeConfig.public.BASE_API_URL,
      headers: {
        'Accept-Language': 'en-US',
        'Accept': 'application/json',
        'x-api-key': runtimeConfig.public.X_API_KEY,
        // 'Content-Type': 'multipart/form-data; boundary=${data._boundary}',
      },

    }
    this.$_fetchFormData = $fetch.create(fetchOptionsFormData)
  }

  /**
    * The HTTP client is utilized to control the process of making API requests.
    * @param method the HTTP method (GET, POST, ...)
    * @param url the endpoint url
    * @param data the body data
    * @param fetchOptions fetch options
    * @returns
    */
  async callFormData(
    method: string,
    url: string,
    data?: object,
    params?: object,
    fetchOptions?: FetchOptions<'json'>,
  ): Promise<T> {
    const prefs = usePrefStore()

    const formData = new FormData()
    const object = data as any
    Object.keys(object).forEach((key) => {
      if (Object.prototype.toString.call(object[key]) === '[object Array]') {
        formData.append(key, JSON.stringify(object[key]))
      }
      else if (Object.prototype.toString.call(object[key]) === '[object FileList]') {
        Object.keys(object[key]).forEach((pos) => {
          formData.append(`${key}[]`, object[key][pos])
        })
      }
      else {
        formData.append(key, object[key])
      }
    })

    return this.$_fetchFormData<T>(
      url,
      {
        method,
        body: formData,
        params,
        ...fetchOptions,
        headers: {
          Authorization: `Bearer ${prefs.accessToken}`,
        },
      },
    )
  }

  /**
     * The HTTP client is utilized to control the process of making API requests.
     * @param method the HTTP method (GET, POST, ...)
     * @param url the endpoint url
     * @param data the body data
     * @param fetchOptions fetch options
     * @returns
     */
  async call(
    method: string,
    url: string,
    data?: object,
    params?: object,
    fetchOptions?: FetchOptions<'json'>,
  ): Promise<T> {
    const prefs = usePrefStore()
    return this.$_fetch<T>(
      url,
      {
        method,
        body: data,
        params,
        ...fetchOptions,
        headers: {
          Authorization: `Bearer ${prefs.accessToken}`,
        },
      },
    )
  }
}

export default FetchFactory

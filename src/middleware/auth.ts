export default defineNuxtRouteMiddleware((to, from) => {
  const prefs = usePrefStore()

  if (to.path === '/')
    return navigateTo('/dashboard')

  if ((prefs.authenticated) && (to.path === '/auth'))
    return navigateTo('/dashboard')

  if (!(prefs.authenticated) && to.path !== '/auth')
    return navigateTo('/auth')
})
